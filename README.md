# React Weather App

Welcome to the React Weather App! This application allows you to create, delete, update, and read users, as well as check the weather forecast for user location using OpenStreetMap and React Leaflet. It is designed to be completely responsive, ensuring a seamless experience across various devices and screen sizes.

## Screenshots

![User List Page](./src/assets/users.png)
![User Weather Page](./src/assets/weather.png)
![Create and Edit Modal](./src/assets/modal.png)

## Installation

To run this project locally, follow these steps:

1. Clone the repository to your local machine:

    ```
    git clone https://gitlab.com/dacost.dev/react-weather-app.git
    ```

2. Navigate to the project directory:

    ```
    cd react-weather-app
    ```

3. Install dependencies using yarn:

    ```
    yarn install
    ```

## Usage

Once the dependencies are installed, you can start the development server by running:

```
yarn start
```

This will start the development server and open the application in your default web browser. You can then navigate to `http://localhost:3000` to view the app.

## Technologies Used

- [React](https://reactjs.org/)
- [OpenStreetMap](https://www.openstreetmap.org/)
- [React Leaflet](https://react-leaflet.js.org/)
- [Redux Toolkit Query (RTK Query)](https://redux-toolkit.js.org/rtk-query/overview)
- [Redux Persist](https://github.com/rt2zz/redux-persist)
- [Material-UI](https://material-ui.com/)

## Credits

This project was created by dacost.dev@gmail.com.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.