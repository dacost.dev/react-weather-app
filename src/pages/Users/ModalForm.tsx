import React from "react";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";

const styles = {
  button: {
    color: "var(--secondary-color)",
  },
};

interface Props {
  handleClose: () => void;
  open: boolean;
  title: string;
  description: React.ReactElement;
  submitBtn: React.ReactElement;
}
const ModalForm = ({
  handleClose,
  open,
  title,
  description,
  submitBtn,
}: Props) => (
  <div className="forms">
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
      sx={{ backgroundColor: "var(--tertiary-color)" }}
    >
      <DialogTitle
        id="alert-dialog-title"
        sx={{ backgroundColor: "var(--tertiary-color)" }}
      >
        {title.toUpperCase()}
      </DialogTitle>
      <DialogContent sx={{ backgroundColor: "var(--tertiary-color)" }}>
        {description}
      </DialogContent>
      <DialogActions sx={{ backgroundColor: "var(--tertiary-color)" }}>
        <Button onClick={handleClose} style={styles.button}>
          Cancel
        </Button>
        {submitBtn}
      </DialogActions>
    </Dialog>
  </div>
);

export default ModalForm;
