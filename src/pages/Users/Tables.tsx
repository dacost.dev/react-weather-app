import React from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Delete, Edit, Cloud } from "@mui/icons-material";
import { IconButton } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useDeleteUserMutation } from "../../api/userSlice";

interface Props {
  rows: { id: string; name: string; latLong: number[] }[];
  refetchUser: () => void;
  setId: (id: string) => void;
  openModal: (result: boolean) => void;
}

const Tables = ({ rows, refetchUser, setId, openModal = () => {} }: Props) => {
  const [deleteUser] = useDeleteUserMutation();
  const handleDelete = (id: string) => {
    deleteUser({ id });
    refetchUser();
  };

  const handleEdit = (id: string) => {
    setId(id);
    openModal(true);
    refetchUser();
  };

  const navigate = useNavigate();
  return (
    <>
      <TableContainer
        component={Paper}
        sx={{
          alignSelf: "start",
          maxHeight: "500px",
        }}
      >
        <Table
          sx={{
            minWidth: 600,
            maxHeight: "500px",
            boxShadow: "0 0 0 0",
            border: "none",
          }}
          aria-label="User Table"
        >
          <TableHead>
            <TableRow>
              <TableCell>Id</TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Latitude</TableCell>
              <TableCell>Longitude</TableCell>
              <TableCell>Weather</TableCell>
              <TableCell>Edit</TableCell>
              <TableCell>Delete</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map(
              (row: { id: string; name: string; latLong: number[] }) => (
                <TableRow key={row.id + row.name}>
                  <TableCell>{row.id}</TableCell>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell>{row.latLong[0]}</TableCell>
                  <TableCell>{row.latLong[1]}</TableCell>
                  <TableCell component="th" scope="row">
                    <IconButton
                      onClick={() => {
                        refetchUser();
                        navigate(`/weather/${row.id}`);
                      }}
                    >
                      <Cloud style={{ color: "#016A70" }} />
                    </IconButton>
                  </TableCell>
                  <TableCell component="th" scope="row">
                    <IconButton onClick={() => handleEdit(row.id)}>
                      <Edit style={{ color: "var(--secondary-color)" }} />
                    </IconButton>
                  </TableCell>
                  <TableCell component="th" scope="row">
                    <IconButton onClick={() => handleDelete(row.id)}>
                      <Delete style={{ color: "var(--secondary-color)" }} />
                    </IconButton>
                  </TableCell>
                </TableRow>
              )
            )}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
};

export default Tables;
