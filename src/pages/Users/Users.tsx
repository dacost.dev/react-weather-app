import React, { useEffect, useState, useCallback } from "react";
import {
  useGetUsersQuery,
  useCreateUserMutation,
  useEditUserMutation,
} from "../../api/userSlice";
import { Add } from "@mui/icons-material";
import { Button } from "@mui/material";
import Tables from "./Tables";
import ModalForm from "./ModalForm";
import TextField from "@mui/material/TextField";
import { v4 as uuidv4 } from "uuid";
import MapComponent from "../../components/Map";

const styles = {
  usersContainer: {
    height: "100%",
    display: "grid",
    justifyContent: "center",
    alignItems: "center",
  },
  tableContainer: {
    backgroundColor: "var(--tertiary-color)",
    padding: "55px",
    borderRadius: "55px",
    boxShadow: "rgb(0 0 0 / 18%) 0px 0px 8px 8px",
  },
  headerContainer: {
    display: "grid",
    gridAutoFlow: "column",
    justifyContent: "space-between",
    alignSelf: "end",
    color: "var(--quaternary-color)",
    marginBottom: "55px",
  },
  button: {
    color: "var(--secondary-color)",
  },
  inputContainer: {
    display: "grid",
    marginTop: "21px",
    width: "100%",
    backgroundColor: "var(--tertiary-color)",
  },
};

function createData(id: string, name: string, latLong: Array<number>) {
  return { id, name, latLong };
}

const Users = () => {
  const [openModalCreate, setOpenModalCreate] = React.useState(false);
  const [openModalEdit, setOpenModalEdit] = React.useState(false);
  const [name, setName] = useState("");
  const [id, setId] = useState("");
  const [latLong, setLatLong] = useState<[number, number]>([
    19.4326018, -99.1332049,
  ]);
  const [rows, setRows] = useState([
    createData(
      "a1c8dbf5-90fe-4228-b1df-a39626f93f09",
      "John Snow",
      [19.432888834307388, -99.13316309452058]
    ),
    createData(
      "f16dad95-91c1-49ce-914a-c0b569e6a7a6",
      "Tyrion Targaryen",
      [19.425153718960143, -98.71181488037111]
    ),
  ]);

  const {
    data: userList,
    isError,
    isLoading,
    refetch: refetchUser,
  } = useGetUsersQuery("");

  const [createUser] = useCreateUserMutation();
  const [editUser] = useEditUserMutation();

  useEffect(() => {
    if (userList && !isError && !isLoading) {
      setRows(userList);
      localStorage.setItem("userList", JSON.stringify(userList));
    } else {
      localStorage.setItem("userList", JSON.stringify(rows));
    }
  }, [isError, isLoading, refetchUser, rows, userList]);

  const handleClose = useCallback(() => {
    setOpenModalEdit(false);
    setOpenModalCreate(false);
  }, []);

  const handleNameChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setName(e.target.value);
    },
    []
  );

  const handleSubmit = useCallback(
    (eventName: string) => {
      try {
        const uuid = uuidv4();
        eventName === "create"
          ? createUser({ id: uuid, name, latLong })
          : editUser({ id, name, latLong });

        setId("");
        setName("");
        setLatLong([19.432888834307388, -99.13316309452058]);
        refetchUser();
        handleClose();
      } catch (error) {
        console.error("Failed to create user:", error);
      }
    },
    [createUser, editUser, handleClose, id, latLong, name, refetchUser]
  );

  return (
    <>
      <ModalForm
        handleClose={handleClose}
        open={openModalCreate}
        title="New User"
        description={
          <div style={styles.inputContainer}>
            <TextField
              required
              id="outlined-primary"
              label="Name"
              defaultValue=""
              onChange={handleNameChange}
              sx={{ borderColor: "var(--secondary-color)" }}
            />
            <div style={{ width: "400px" }}>
              <MapComponent
                markerPosition={latLong}
                setMarkerPosition={setLatLong}
              />
            </div>
          </div>
        }
        submitBtn={
          <Button
            onClick={() => handleSubmit("create")}
            autoFocus
            style={{
              color:
                name.length === 0
                  ? "rgb(1 106 112 / 39%)"
                  : "var(--secondary-color)",
            }}
            disabled={name.length === 0}
          >
            Add
          </Button>
        }
      />
      <ModalForm
        handleClose={handleClose}
        open={openModalEdit}
        title="Edit User"
        description={
          <div style={styles.inputContainer}>
            <TextField
              required
              id="outlined-primary"
              label="Name"
              defaultValue=""
              onChange={handleNameChange}
              sx={{ borderColor: "var(--secondary-color)" }}
            />
            <div style={{ width: "400px" }}>
              <MapComponent
                markerPosition={latLong}
                setMarkerPosition={setLatLong}
              />
            </div>
          </div>
        }
        submitBtn={
          <Button
            onClick={() => handleSubmit("delete")}
            autoFocus
            style={{
              color:
                name.length === 0
                  ? "rgb(1 106 112 / 39%)"
                  : "var(--secondary-color)",
            }}
            disabled={name.length === 0}
          >
            Update
          </Button>
        }
      />
      {isLoading && !isError ? <div>Loading...</div> : null}
      {isError ? <div>Error...</div> : null}
      {rows ? (
        <div style={styles.usersContainer}>
          <div className="tableContainer" style={styles.tableContainer}>
            <div style={styles.headerContainer}>
              <h2>User List</h2>
              <Button onClick={() => setOpenModalCreate(true)}>
                <div style={styles.button}>
                  Add User
                  <Add />
                </div>
              </Button>
            </div>
            <Tables
              rows={rows}
              refetchUser={refetchUser}
              setId={setId}
              openModal={setOpenModalEdit}
            />
          </div>
        </div>
      ) : null}
    </>
  );
};

export default Users;
