/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import ThermostatIcon from "@mui/icons-material/Thermostat";
import WaterDropIcon from "@mui/icons-material/WaterDrop";
import AirIcon from "@mui/icons-material/Air";
import WhatshotIcon from "@mui/icons-material/Whatshot";
import AcUnitIcon from "@mui/icons-material/AcUnit";
import TodayIcon from "@mui/icons-material/Today";

const styles = {
  icon: {
    color: "var(--secondary-color)",
  },
  gridColumn: {
    display: "grid",
    gridAutoFlow: "column",
    alignItems: "center",
  },
  inContainer: { display: "grid", gap: "21px" },
  outContainer: {
    display: "grid",
    gridAutoFlow: "column",
  },
  gridCardContainer: {
    display: "grid",
    gridAutoFlow: "column",
    justifyContent: "space-between",
  },
  herderTemp: { display: "grid", justifyContent: "end" },
};

const Forecast = ({ forecast }: any) => {
  return (
    <div style={{ display: "grid" }}>
      <div className="forecast" style={{ display: "grid", gap: "55px" }}>
        {forecast.map(
          (
            el: {
              date: any;
              indexDay: { temp: any; humidity: any; wind: any };
              indexNight: { temp: any; humidity: any; wind: any };
            },
            index: any
          ) => {
            return (
              <div key={index + el.date} style={styles.inContainer}>
                <div style={styles.herderTemp}>
                  {el.indexDay.temp.split(".")[0] < 20 ? (
                    <div style={styles.gridCardContainer}>
                      <div style={styles.outContainer}>
                        COLD
                        <AcUnitIcon />
                      </div>
                    </div>
                  ) : (
                    <div style={styles.outContainer}>
                      HOT
                      <WhatshotIcon />
                    </div>
                  )}
                </div>
                <div style={styles.outContainer}>
                  <div style={styles.inContainer}>
                    <div style={styles.gridColumn}>
                      <TodayIcon style={styles.icon} />
                      {`${el.date}`}
                    </div>
                    <div style={styles.gridColumn}>
                      <ThermostatIcon style={styles.icon} />
                      {` ${el.indexDay.temp}`}
                    </div>
                    <div style={styles.gridColumn}>
                      <WaterDropIcon style={styles.icon} />
                      {`${el.indexDay.humidity}`}
                    </div>
                    <div style={styles.gridColumn}>
                      <AirIcon style={styles.icon} />
                      {`${el.indexDay.wind}`}
                    </div>
                  </div>
                </div>
              </div>
            );
          }
        )}
      </div>
    </div>
  );
};

export default Forecast;
