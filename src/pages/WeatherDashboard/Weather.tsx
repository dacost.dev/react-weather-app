/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import ThermostatIcon from "@mui/icons-material/Thermostat";
import WaterDropIcon from "@mui/icons-material/WaterDrop";
import AirIcon from "@mui/icons-material/Air";
import WhatshotIcon from "@mui/icons-material/Whatshot";
import AcUnitIcon from "@mui/icons-material/AcUnit";
import TodayIcon from "@mui/icons-material/Today";

const Weather = ({ currentWeaher, forecast, date }: any) => {
  return (
    <div>
      <div
        style={{
          display: "grid",
          gap: "21px",
          justifyContent: "center",
          width: "100%",
        }}
      >
        <div style={{ display: "grid", justifyContent: "end" }}>
          {currentWeaher.hourly.temperature_2m[
            currentWeaher.hourly.time.indexOf(date)
          ] < 20 ? (
            <div style={{ display: "grid", gridAutoFlow: "column" }}>
              COLD
              <AcUnitIcon />
            </div>
          ) : (
            <div style={{ display: "grid", gridAutoFlow: "column" }}>
              HOT
              <WhatshotIcon />
            </div>
          )}
        </div>
        <div
          style={{
            display: "grid",
            gridAutoFlow: "column",
            alignItems: "center",
            gap: "21px",
          }}
        >
          {
            <>
              Date
              <TodayIcon style={{ color: "var(--secondary-color)" }} />
            </>
          }

          {`${date.split("T")[0]}`}
        </div>
        <div
          style={{
            display: "grid",
            gridAutoFlow: "column",
            alignItems: "center",
            gap: "21px",
          }}
        >
          {
            <>
              Temperature
              <ThermostatIcon style={{ color: "var(--secondary-color)" }} />
            </>
          }

          {`${
            currentWeaher.hourly.temperature_2m[
              currentWeaher.hourly.time.indexOf(date)
            ]
          } ${forecast[0].units.temperature_2m}`}
        </div>
        <div
          style={{
            display: "grid",
            gridAutoFlow: "column",
            alignItems: "center",
            gap: "21px",
          }}
        >
          {
            <>
              Humidity
              <WaterDropIcon style={{ color: "var(--secondary-color)" }} />
            </>
          }

          {`${
            currentWeaher.hourly.relative_humidity_2m[
              currentWeaher.hourly.time.indexOf(date)
            ]
          } ${forecast[0].units.relative_humidity_2m}`}
        </div>

        <div
          style={{
            display: "grid",
            gridAutoFlow: "column",
            alignItems: "center",
            gap: "21px",
          }}
        >
          {
            <>
              Wind speed
              <AirIcon style={{ color: "var(--secondary-color)" }} />
            </>
          }

          {`${
            currentWeaher.hourly.wind_speed_10m[
              currentWeaher.hourly.time.indexOf(date)
            ]
          } ${forecast[0].units.wind_speed_10m}`}
        </div>
      </div>
    </div>
  );
};

export default Weather;
