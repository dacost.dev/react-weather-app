/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useEffect, useState } from "react";
import MapComponent from "../../components/Map";
import Card from "../../components/Card";
import Grid from "../../components/Grid";
import { IconButton } from "@mui/material";
import KeyboardReturnIcon from "@mui/icons-material/KeyboardReturn";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import DateRangeIcon from "@mui/icons-material/DateRange";
import CloudQueueIcon from "@mui/icons-material/CloudQueue";
import { useGetUserQuery } from "../../api/userSlice";
import { useParams, useNavigate } from "react-router-dom";
import Weather from "./Weather";
import Forecast from "./Forecast";

interface CardTitles {
  name: string;
  icon: React.ReactElement;
  style: { gridArea: string };
  description: React.ReactElement;
}

const WeatherDashboard = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [currentWeaher, setCurrentWeaher] = useState(null);
  const [forecast, setForecast] = useState<any>(null);
  const { data: userData, isError, isLoading, refetch } = useGetUserQuery(id);
  const date = new Date().toISOString().slice(0, -10) + "00";
  const [geoPoint, setGeopoint] = useState<number[]>([]);

  useEffect(() => {
    refetch();
    if (userData) {
      setGeopoint([userData[0].latLong[0], userData[0].latLong[1]]);
      fetch(
        `${process.env.REACT_APP_WEATHER_API_URL}forecast?latitude=${userData[0].latLong[0]}&longitude=${userData[0].latLong[1]}&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m`
      )
        .then((response) => response.json())
        .then((data) => {
          const current = date.split("T");
          const day = current[0].split("-");
          const nextDays = [];

          for (let index = 1; index < 6; index++) {
            const dateR = `${day[0]}-${day[1]}-${
              Number(day[2]) + index < 10
                ? `0${Number(day[2]) + index}`
                : Number(day[2]) + index
            }`;

            const dayString = data.hourly.time.indexOf(`${dateR}T17:00`);

            nextDays.push({
              date: dateR,
              indexDay: {
                date: `${dateR}`,
                temp:
                  data.hourly.temperature_2m[dayString] +
                  ` ${data.hourly_units.temperature_2m}`,
                humidity:
                  data.hourly.relative_humidity_2m[dayString] +
                  ` ${data.hourly_units.relative_humidity_2m}`,
                wind:
                  data.hourly.wind_speed_10m[dayString] +
                  ` ${data.hourly_units.wind_speed_10m}`,
              },
              units: data.hourly_units,
            });
          }
          setCurrentWeaher(data);
          setForecast(nextDays);
        });
    }
  }, [date, refetch, userData]);

  const cardsTitles: CardTitles[] = [
    {
      name: "Profile",
      icon: <AccountCircleIcon style={{ color: "var(--secondary-color)" }} />,
      style: { gridArea: "a" },
      description: (
        <div>
          {userData ? (
            <ul
              style={{ display: "grid", gap: "21px", justifyContent: "center" }}
            >
              <li>{`Name: ${userData[0].name}`}</li>
              <li>{`Lat: ${userData[0].latLong[0]}`}</li>
              <li>{`Long: ${userData[0].latLong[1]}`}</li>
            </ul>
          ) : null}
        </div>
      ),
    },
    {
      name: "Weather",
      icon: <CloudQueueIcon style={{ color: "var(--secondary-color)" }} />,
      style: { gridArea: "b" },
      description: (
        <div>
          {currentWeaher && forecast ? (
            <Weather
              currentWeaher={currentWeaher}
              forecast={forecast}
              date={date}
            />
          ) : null}
        </div>
      ),
    },
    {
      name: "Forecast",
      icon: <DateRangeIcon style={{ color: "var(--secondary-color)" }} />,
      style: { gridArea: "c" },
      description: (
        <div>{forecast ? <Forecast forecast={forecast} /> : null}</div>
      ),
    },
  ];

  return (
    <>
      {isLoading && !isError ? <div>Loading...</div> : null}
      {isError ? <div>Error...</div> : null}
      {userData && geoPoint.length > 0 ? (
        <>
          <MapComponent
            markerPosition={[userData[0].latLong[0], userData[0].latLong[1]]}
            setMarkerPosition={() => refetch}
          />
          <div
            style={{
              display: "grid",
              justifyContent: "start",
              alignItems: "center",
              margin: "21px 0px 0px 21px",
            }}
          >
            <IconButton onClick={() => navigate("/")}>
              <KeyboardReturnIcon style={{ color: "var(--secondary-color)" }} />
            </IconButton>
          </div>
          <Grid>
            <>
              {cardsTitles.map((element, index) => (
                <Card
                  key={index + element.name}
                  title={element.name}
                  Icon={element.icon}
                  style={element.style}
                  description={element.description}
                />
              ))}
            </>
          </Grid>
        </>
      ) : null}
    </>
  );
};

export default WeatherDashboard;
