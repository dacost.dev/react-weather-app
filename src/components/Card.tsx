import React from "react";

const styles = {
  cardContainer: {
    backgroundColor: "var(--tertiary-color)",
    padding: "55px",
    borderRadius: "55px",
    zIndex: "999px",
    boxShadow: "0px 0px 8px 8px rgb(0 0 0 / 31%)",
    marginBottom: "21px",
  },
  titleContainer: {
    display: "grid",
    gridAutoFlow: "column",
    columnGap: "21px",
    justifyContent: "center",
  },
  title: { fontSize: "21px", fontWeight: "bold" },
  image: { width: "144px", borderRadius: "55px", padding: "8px" },
  body: {
    display: "grid",
    gridAutoFlow: "column",
    height: "100%",
    gridGap: "21px",
  },
};

interface Props {
  title: string;
  Icon: React.ReactElement;
  description: React.ReactElement | string;
  style: { gridArea: string };
}

const Card = ({ title, Icon, description, style }: Props) => {
  return (
    <div style={{ ...styles.cardContainer, ...style }}>
      <div style={styles.titleContainer}>
        {Icon}
        <div style={styles.title}>{title}</div>
      </div>
      <hr style={{ marginTop: "21px" }} />
      <div style={styles.body}>
        <div>{description}</div>
      </div>
    </div>
  );
};

export default Card;
