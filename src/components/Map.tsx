/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useState } from "react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { DragEndEvent, Icon } from "leaflet";
import iconMarker from "./../assets/marker.svg";

const markerIcon = new Icon({
  iconUrl: iconMarker,
  iconSize: [55, 55],
});

const MapComponent = ({ markerPosition, setMarkerPosition }: any) => {
  const [marker, setMarker] = useState(markerPosition);
  const handleMarkerDragEnd = (event: DragEndEvent) => {
    const markerGeoPoint = [
      event.target.getLatLng().lat,
      event.target.getLatLng().lng,
    ];
    setMarkerPosition(markerGeoPoint);
    setMarker(markerGeoPoint);
  };

  return (
    <>
      {marker ? (
        <MapContainer
          center={marker}
          zoom={13}
          style={{
            width: "100%",
            height: "377px",
            paddingTop: "55px",
            position: "relative",
            marginTop: "55px",
          }}
        >
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          />
          <Marker
            position={markerPosition}
            draggable={true}
            eventHandlers={{ dragend: handleMarkerDragEnd }}
            icon={markerIcon}
          >
            <Popup>
              Marker Position: {markerPosition[0]}, {markerPosition[1]}
            </Popup>
          </Marker>
        </MapContainer>
      ) : null}
    </>
  );
};

export default MapComponent;
