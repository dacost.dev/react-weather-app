import React from "react";

interface Props {
  children: React.ReactNode;
}

const Grid: React.FC<Props> = ({ children }) => {
  return (
    <div
      className="gridResponsive"
      style={{
        display: "grid",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        height: "40%",
        gap: "89px 21px",
        marginTop: "0px",
        paddingTop: "8px",
        position: "absolute",
      }}
    >
      {children}
    </div>
  );
};

export default Grid;
