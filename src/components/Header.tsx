import * as React from "react";
import { AppBar, Typography, Container } from "@mui/material";
import AcUnitIcon from "@mui/icons-material/AcUnit";

function Header() {
  return (
    <AppBar
      position="fixed"
      style={{ backgroundColor: "var(--secondary-color)", height: "55px" }}
    >
      <Container
        sx={{
          marginLeft: "21px",
        }}
      >
        <div
          style={{
            display: "grid",
            gridAutoFlow: "column",
            alignItems: "center",
            justifyContent: "center",
            width: "fit-content",
            height: "55px",
          }}
        >
          <AcUnitIcon
            sx={{
              display: { xs: "flex", md: "flex" },
              mr: 1,
              color: "var(--tertiary-color)",
            }}
          />
          <Typography
            variant="h6"
            noWrap
            component="a"
            href="#app-bar-with-responsive-menu"
            sx={{
              mr: 1,
              display: { xs: "flex", md: "flex" },
              fontFamily: "monospace",
              fontWeight: 700,
              letterSpacing: ".3rem",
              color: "whitesmoke",
              textDecoration: "none",
            }}
          >
            Weather App
          </Typography>
        </div>
      </Container>
    </AppBar>
  );
}
export default Header;
