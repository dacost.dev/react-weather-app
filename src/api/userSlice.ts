import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

interface User {
  id: string;
  name: string;
  latLong: number[];
}

interface UserState {
  users: User[];
}

const initialState: UserState = {
  users: [],
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    setUsers: (state, action: PayloadAction<User[]>) => {
      state.users = action.payload;
    },
    addUser: (state, action: PayloadAction<User>) => {
      const newUser: User = { ...action.payload };
      state.users.push(newUser);
    },
    deleteUser: (state, action: PayloadAction<string>) => {
      state.users = state.users.filter((user) => user.id !== action.payload);
    },
  },
});

export const { setUsers, addUser, deleteUser } = userSlice.actions;

export const reducer = userSlice.reducer;

export const usersApi = createApi({
  reducerPath: "usersApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "/api",
  }),
  endpoints: (builder) => ({
    getUsers: builder.query({
      queryFn: () => {
        return { data: JSON.parse(String(localStorage.getItem("userList"))) };
      },
    }),
    getUser: builder.query({
      queryFn: (id) => {
        return {
          data: JSON.parse(String(localStorage.getItem("userList"))).filter(
            (user: { id: string }) => user.id === id
          ),
        };
      },
    }),
    createUser: builder.mutation({
      queryFn: (userData) => {
        const currentData = JSON.parse(
          String(localStorage.getItem("userList"))
        );
        currentData.push(userData);
        localStorage.setItem("userList", JSON.stringify(currentData));

        return JSON.parse(String(localStorage.getItem("userList")));
      },
    }),

    deleteUser: builder.mutation({
      queryFn: (useId) => {
        const currentData = JSON.parse(
          String(localStorage.getItem("userList"))
        );
        localStorage.setItem(
          "userList",
          JSON.stringify(
            currentData.filter(
              (user: { id: string }) => String(user.id) !== String(useId.id)
            )
          )
        );
        return JSON.parse(String(localStorage.getItem("userList")));
      },
    }),

    editUser: builder.mutation({
      queryFn: (userData) => {
        const currentData = JSON.parse(
          String(localStorage.getItem("userList"))
        );
        const updateObjectInArray = (
          array: [{ id: string; name: string; latLong: number[] }],
          idToUpdate: string,
          updatedProperties: { name: string; latLong: number[] }
        ) => {
          const index = array.findIndex(
            (user: { id: string }) => user.id === idToUpdate
          );
          if (index !== -1) {
            array[index] = { ...array[index], ...updatedProperties };
          }
          return array;
        };

        localStorage.setItem(
          "userList",
          JSON.stringify(
            updateObjectInArray(currentData, userData.id, {
              name: userData.name,
              latLong: userData.latLong,
            })
          )
        );

        return JSON.parse(String(localStorage.getItem("userList")));
      },
    }),
  }),
});

export const {
  useGetUsersQuery,
  useGetUserQuery,
  useCreateUserMutation,
  useDeleteUserMutation,
  useEditUserMutation,
} = usersApi;
