import React from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Users from "../pages/Users/Users";
import WeatherDashboard from "../pages/WeatherDashboard/WeatherDashboard";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Users />,
  },
  {
    path: "/weather/:id",
    element: <WeatherDashboard />,
  },
]);

function Router() {
  return <RouterProvider router={router} />;
}

export default Router;
