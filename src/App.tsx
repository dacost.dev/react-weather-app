import React from "react";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { store, persistor } from "./api/store";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import Router from "./routes/Router";
import Header from "./components/Header";
import axios from "axios";
import CssBaseline from "@mui/material/CssBaseline";
import "leaflet/dist/images/marker-icon.png";
import "leaflet/dist/leaflet.css";

axios.defaults.withCredentials = true;
const darkTheme = createTheme({
  palette: {
    mode: "dark",
  },
});

const App = () => {
  return (
    <div className="App">
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <ThemeProvider theme={darkTheme}>
            <CssBaseline />
            <Header />
            <Router />
          </ThemeProvider>
        </PersistGate>
      </Provider>
    </div>
  );
};

export default App;
